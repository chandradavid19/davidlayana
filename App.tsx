import React from 'react';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import Main from './Main';
import Toast from 'react-native-toast-message';
import useDatabase from './src/utils/useDatabase';

const App = () => {
  useDatabase();

  return (
    <GestureHandlerRootView style={{flex: 1}}>
      <Main />
      <Toast />
    </GestureHandlerRootView>
  );
};

export default App;
