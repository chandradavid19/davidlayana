import React from 'react';
import RootNavigation from './src/navigator/RootNavigation';

const Main = () => {
  return <RootNavigation />;
};

export default Main;
