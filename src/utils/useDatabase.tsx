import {useEffect} from 'react';
import SQLite from 'react-native-sqlite-storage';

const useDatabase = () => {
  useEffect(() => {
    const db = SQLite.openDatabase({
      name: 'eve.db',
      location: 'default',
    });

    db.transaction(tx => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS Users (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, email TEXT, phoneNumber TEXT, password TEXT)',
        [],
        (tx, results) => {
          console.log('Table created or already exists:', results);
        },
        error => {
          console.log('Error creating table:', error);
        },
      );
      tx.executeSql(
        `CREATE TABLE IF NOT EXISTS Sessions (id INTEGER PRIMARY KEY AUTOINCREMENT, userId INTEGER, token TEXT,FOREIGN KEY (userId) REFERENCES Users (id))`,
        [],
        (tx, results) => {
          console.log('Sessions table created or already exists:', results);
        },
        error => {
          console.log('Error creating Sessions table:', error);
        },
      );
    });

    return () => {
      db.close();
    };
  }, []);

  return SQLite.openDatabase({name: 'eve.db', location: 'default'});
};

export default useDatabase;
