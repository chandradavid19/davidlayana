import Toast from 'react-native-toast-message';

export const showToast = (type, text) => {
  Toast.show({
    type: type,
    text1: text,
    visibilityTime: 3000,
    autoHide: true,
    topOffset: 30,
    bottomOffset: 40,
    position: 'top',
  });
};

export const showSuccessToast = (text: string) => {
  showToast('success', text);
};

export const showErrorToast = (text: string) => {
  showToast('error', text);
};
