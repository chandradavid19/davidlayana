import {StyleSheet, Platform} from 'react-native';

const isAndroid = Platform.OS === 'android';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    padding: 18,
  },
  registerContainer: {
    paddingHorizontal: 5,
    marginBottom: 24,
    marginTop: isAndroid ? 20 : 0,
  },
  registerText: {
    color: '#0F172A',
    fontSize: 28,
    fontWeight: '600',
    lineHeight: 50,
  },
  descriptionText: {
    color: '#6B7280',
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 25,
  },
});
