import React from 'react';
import {View, Text, Animated} from 'react-native';

import {styles} from './Register.style';

import {useRegisterHook} from './Register.hook';

import RegisterComponent from '../../components/Register/Register';
import Screen from '../../components/Screen/Screen';

export default function Register() {
  const {handleClickRegister, handleClickLogin, fadeIn} = useRegisterHook();
  return (
    <Screen>
      <Animated.View style={[styles.container, {opacity: fadeIn}]}>
        <View style={styles.registerContainer}>
          <Text style={styles.registerText}>Register here</Text>
          <Text style={styles.descriptionText}>
            Please enter your data to complete your account registration process
          </Text>
        </View>
        <RegisterComponent
          onSubmit={handleClickRegister}
          handleClickLogin={handleClickLogin}
        />
      </Animated.View>
    </Screen>
  );
}
