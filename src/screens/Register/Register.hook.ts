import {useCallback, useEffect, useRef} from 'react';
import {Animated, Easing} from 'react-native';
import {
  RootStackParamList,
  StackNavigationProp,
} from '../../navigator/interface.routes';
import {useNavigation} from '@react-navigation/native';
import SQLite from 'react-native-sqlite-storage';

import {showSuccessToast, showErrorToast} from '../../utils/useToast';

export const useRegisterHook = () => {
  const navigation = useNavigation<StackNavigationProp<RootStackParamList>>();

  const fadeIn = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.timing(fadeIn, {
      toValue: 1,
      duration: 1000,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start();
  }, [fadeIn]);

  const checkIfEmailExists = (email, callback) => {
    const db = SQLite.openDatabase({
      name: 'eve.db',
      location: 'default',
    });

    db.transaction(tx => {
      tx.executeSql(
        'SELECT COUNT(*) as count FROM Users WHERE email = ?',
        [email],
        (_tx, results) => {
          const count = results.rows.item(0).count;
          callback(count > 0);
        },
        error => {
          console.error('Error checking email:', error);
          callback(false);
        },
      );
    });
  };

  const insertUserIntoDatabase = (name, email, phoneNumber, password) => {
    const db = SQLite.openDatabase({
      name: 'eve.db',
      location: 'default',
    });

    db.transaction(tx => {
      tx.executeSql(
        'INSERT INTO Users (name, email, phoneNumber, password) VALUES (?, ?, ?, ?)',
        [name, email, phoneNumber, password],
        (_tx, results) => {
          console.log('User registered successfully', results);
          showSuccessToast('Registration successful!');
          navigation.goBack();
        },
        error => {
          console.log('Error registering user:', error);
          showErrorToast('There are several errors, try again later.');
        },
      );
    });
  };

  const handleClickRegister = (
    name: string,
    email: string,
    phoneNumber: string,
    password: string,
  ) => {
    const payload = {
      name: name,
      email: email,
      phoneNumber: phoneNumber.replace(/-/g, ''),
      password: password,
    };
    checkIfEmailExists(email, emailExists => {
      if (emailExists) {
        showErrorToast('Email has been used, please use another email.');
      } else {
        insertUserIntoDatabase(
          payload.name,
          payload.email,
          payload.phoneNumber,
          payload.password,
        );
      }
    });
  };

  const handleClickLogin = useCallback(() => {
    navigation.navigate('Login');
  }, [navigation]);

  return {
    handleClickRegister,
    handleClickLogin,
    fadeIn,
  };
};
