import {useCallback} from 'react';
import {
  RootStackParamList,
  StackNavigationProp,
} from '../../navigator/interface.routes';
import {useNavigation} from '@react-navigation/native';

import {showSuccessToast} from '../../utils/useToast';
import useDatabase from '../../utils/useDatabase';

export const useHomepageHook = () => {
  const navigation = useNavigation<StackNavigationProp<RootStackParamList>>();
  const db = useDatabase();

  const clearSessions = () => {
    db.transaction(tx => {
      tx.executeSql(
        'DELETE FROM Sessions',
        [],
        (_tx, results) => {
          console.log('Sessions cleared successfully:', results);
          navigation.navigate('Login');
          showSuccessToast('Logout successfully');
        },
        error => {
          console.log('Error clearing Sessions:', error);
        },
      );
    });
  };

  const handleClickLogout = useCallback(() => {
    clearSessions();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return {
    handleClickLogout,
  };
};
