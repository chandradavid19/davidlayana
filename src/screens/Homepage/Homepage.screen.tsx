import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

import {styles} from './Homepage.style';

import {useHomepageHook} from './Homepage.hook';

export default function Homepage() {
  const {handleClickLogout} = useHomepageHook();

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.logoutButton} onPress={handleClickLogout}>
        <Text style={styles.logoutButtonText}>Logout</Text>
      </TouchableOpacity>
    </View>
  );
}
