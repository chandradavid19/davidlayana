import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#001D3D',
  },
  logoContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sheetContainer: {
    flex: 3,
    padding: 16,
    backgroundColor: '#FFF',
    borderTopLeftRadius: 28,
    borderTopRightRadius: 28,
  },
  welcomeContainer: {
    paddingHorizontal: 5,
  },
  greetingText: {
    color: '#0F172A',
    fontSize: 28,
    fontWeight: '600',
    lineHeight: 50,
  },
  descriptionText: {
    color: '#6B7280',
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 25,
  },
});
