import {useCallback, useEffect, useState, useRef} from 'react';
import {Animated, Easing} from 'react-native';
import {
  RootStackParamList,
  StackNavigationProp,
} from '../../navigator/interface.routes';
import {useNavigation} from '@react-navigation/native';
import SQLite from 'react-native-sqlite-storage';

import {showSuccessToast, showErrorToast} from '../../utils/useToast';

export const useLoginHook = () => {
  const [isSafeToReset, setSafeToReset] = useState<boolean>(false);
  const navigation = useNavigation<StackNavigationProp<RootStackParamList>>();

  const fadeInLogo = useRef(new Animated.Value(0)).current;
  const translateY1 = useRef(new Animated.Value(400)).current;
  const translateY2 = useRef(new Animated.Value(400)).current;
  const translateY3 = useRef(new Animated.Value(400)).current;

  useEffect(() => {
    const animate = (translateY, delay) => {
      Animated.timing(translateY, {
        toValue: 0,
        duration: 800,
        easing: Easing.out(Easing.back(1)),
        delay,
        useNativeDriver: true,
      }).start();
    };
    animate(translateY1, 0);
    animate(translateY2, 100);
    animate(translateY3, 200);
  }, [translateY1, translateY2, translateY3]);

  useEffect(() => {
    Animated.timing(fadeInLogo, {
      toValue: 1,
      duration: 1000,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start();
  }, [fadeInLogo]);

  const handleClickForgotPassword = useCallback(() => {
    console.log('Forgot password clicked');
  }, []);

  const generateToken = () => {
    return (
      Math.random().toString(36).substring(2, 15) +
      Math.random().toString(36).substring(2, 15)
    );
  };

  const saveSessionToken = (userId: string, token: string) => {
    const db = SQLite.openDatabase({
      name: 'eve.db',
      location: 'default',
    });

    db.transaction(tx => {
      tx.executeSql(
        'INSERT INTO Sessions (userId, token) VALUES (?, ?)',
        [userId, token],
        _tx => {
          navigation.reset({
            index: 0,
            routes: [{name: 'Homepage'}],
          });
        },
        error => {
          console.error('Error saving session token:', error);
        },
      );
    });
  };

  const checkLoginCredentials = (email, password, callback) => {
    const db = SQLite.openDatabase({
      name: 'eve.db',
      location: 'default',
    });

    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM Users WHERE email = ?',
        [email],
        (_tx, results) => {
          const user = results.rows.item(0);
          if (user && user.password === password) {
            const userId = user.id;
            const token = generateToken();
            saveSessionToken(userId, token);
            setSafeToReset(true);
            callback(true);
          } else {
            callback(false);
          }
        },
        error => {
          console.error('Error checking login credentials:', error);
          callback(false);
        },
      );
    });
  };

  const handleClickLogin = (email: string, password: string) => {
    checkLoginCredentials(email, password, loginSuccessful => {
      if (loginSuccessful) {
        showSuccessToast('Login successful');
      } else {
        showErrorToast('Invalid email or password');
      }
    });
  };

  const handleClickRegister = useCallback(() => {
    navigation.navigate('Register');
  }, [navigation]);

  return {
    handleClickForgotPassword,
    handleClickLogin,
    handleClickRegister,
    fadeInLogo,
    translateY1,
    translateY2,
    translateY3,
    isSafeToReset,
  };
};
