import React from 'react';
import {View, Animated} from 'react-native';

import {styles} from './Login.style';

import {ImageAssets} from '../../assets/images';

import LoginComponent from '../../components/Login/Login';

import {useLoginHook} from './Login.hook';

export default function Login() {
  const {
    handleClickForgotPassword,
    handleClickLogin,
    handleClickRegister,
    fadeInLogo,
    translateY1,
    translateY2,
    translateY3,
    isSafeToReset,
  } = useLoginHook();

  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <Animated.Image
          source={ImageAssets.img_eve_logo}
          style={{opacity: fadeInLogo}}
        />
      </View>
      <Animated.View style={styles.sheetContainer}>
        <Animated.Text
          style={[
            styles.greetingText,
            {transform: [{translateY: translateY1}]},
          ]}>
          Welcome back 👋
        </Animated.Text>
        <Animated.Text
          style={[
            styles.descriptionText,
            {transform: [{translateY: translateY2}]},
          ]}>
          Please enter your login information below to access your account
        </Animated.Text>
        <LoginComponent
          handleClickForgotPassword={handleClickForgotPassword}
          onSubmit={handleClickLogin}
          handleClickRegister={handleClickRegister}
          translateY3={translateY3}
          isSafeToReset={isSafeToReset}
        />
      </Animated.View>
    </View>
  );
}
