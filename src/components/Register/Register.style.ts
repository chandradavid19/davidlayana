import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  label: {
    color: 'black',
    margin: 20,
    marginLeft: 0,
    marginBottom: 10,
  },
  container: {
    justifyContent: 'center',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#D1D5DB',
    borderRadius: 8,
    marginBottom: 10,
    paddingHorizontal: 8,
  },
  inputNumberContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#D1D5DB',
    borderRadius: 8,
    marginBottom: 10,
  },
  input: {
    flex: 1,
    padding: 10,
    color: '#000',
  },
  icon: {
    marginHorizontal: 10,
  },
  phoneNumberContainer: {
    height: 50,
    backgroundColor: '#D1D5DB',
    borderRadius: 4,
    paddingHorizontal: 10,
    justifyContent: 'center',
  },
  phoneNumberIcon: {
    fontSize: 16,
    color: '#000',
    textAlign: 'center',
  },
  disabledContainer: {
    justifyContent: 'center',
    backgroundColor: '#ECECEC',
  },
  registerButton: {
    backgroundColor: '#003566',
    borderRadius: 8,
    paddingVertical: 17,
    width: '100%',
    marginTop: 20,
  },
  registerButtonDisabled: {
    backgroundColor: '#91a3b0',
    borderRadius: 8,
    paddingVertical: 17,
    width: '100%',
    marginTop: 20,
  },
  registerButtonText: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
    textAlign: 'center',
  },
  loginContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  haveAccountText: {
    textAlign: 'center',
    color: '#6B7280',
    fontSize: 14,
    fontWeight: '600',
  },
  loginButtonText: {
    textAlign: 'center',
    color: '#003566',
    fontSize: 14,
    fontWeight: '600',
  },
  errorText: {
    color: 'red',
    fontSize: 14,
  },
});
