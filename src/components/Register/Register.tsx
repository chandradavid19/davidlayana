import React, {useState} from 'react';
import {Text, View, TextInput, Image, TouchableOpacity} from 'react-native';
import {useForm, Controller} from 'react-hook-form';

import {styles} from './Register.style';

import {IconAssets} from '../../assets/icons';

interface RegisterComponentProps {
  onSubmit: (
    name: string,
    email: string,
    phoneNumber: string,
    password: string,
  ) => void;
  handleClickLogin: () => void;
}

const RegisterComponent: React.FC<RegisterComponentProps> = ({
  onSubmit,
  handleClickLogin,
}) => {
  const {control, handleSubmit, formState, getValues} = useForm({
    defaultValues: {
      name: '',
      email: '',
      phoneNumber: '',
      password: '',
      confirmPassword: '',
    },
  });

  const isSubmitDisabled =
    !formState.isValid ||
    !formState.dirtyFields.name ||
    !formState.dirtyFields.email ||
    !formState.dirtyFields.phoneNumber ||
    !formState.dirtyFields.password ||
    !formState.dirtyFields.confirmPassword;

  const [showPassword, setShowPassword] = useState(false);
  const [passwordMatch, setPasswordMatch] = useState(true);

  const togglePasswordVisibility = () => {
    setShowPassword(prevState => !prevState);
  };

  const formatPhoneNumber = text => {
    const cleanedText = text.replace(/[^0-9]/g, '');

    const formattedText = cleanedText.replace(/(\d{3})(?=\d)/g, '$1-');

    return formattedText;
  };

  const checkPasswordMatch = (confirmPassword: string) => {
    const password = getValues('password');
    setPasswordMatch(password === confirmPassword);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.label}>Name</Text>
      <View style={styles.inputContainer}>
        <Image source={IconAssets.name} />
        <Controller
          control={control}
          render={({field: {onChange, onBlur, value}}) => (
            <TextInput
              style={styles.input}
              onBlur={onBlur}
              onChangeText={value => onChange(value)}
              value={value}
              placeholder="Name"
              placeholderTextColor={'#A9A9AC'}
            />
          )}
          name="name"
          rules={{required: true}}
        />
      </View>
      <Text style={styles.label}>Email</Text>
      <View style={styles.inputContainer}>
        <Image source={IconAssets.email} />
        <Controller
          control={control}
          render={({field: {onChange, onBlur, value}}) => (
            <TextInput
              style={styles.input}
              onBlur={onBlur}
              onChangeText={value => {
                onChange(value);
              }}
              value={value}
              placeholder="Email"
              placeholderTextColor={'#A9A9AC'}
            />
          )}
          name="email"
          rules={{required: true, validate: value => value.includes('@')}}
        />
      </View>
      <Text style={styles.label}>Phone Number</Text>
      <View style={styles.inputNumberContainer}>
        <View style={styles.phoneNumberContainer}>
          <Text style={styles.phoneNumberIcon}>+62</Text>
        </View>
        <Controller
          control={control}
          render={({field: {onChange, onBlur, value}}) => (
            <TextInput
              style={styles.input}
              onBlur={onBlur}
              onChangeText={text => onChange(formatPhoneNumber(text))}
              value={value}
              placeholder="Phone Number"
              placeholderTextColor={'#A9A9AC'}
              keyboardType="numeric"
            />
          )}
          name="phoneNumber"
          rules={{required: true}}
        />
      </View>
      <Text style={styles.label}>Create Password</Text>
      <View style={styles.inputContainer}>
        <Image source={IconAssets.password} />
        <Controller
          control={control}
          render={({field: {onChange, onBlur, value}}) => (
            <TextInput
              style={styles.input}
              onBlur={onBlur}
              onChangeText={value => onChange(value)}
              value={value}
              placeholder="Create Password"
              placeholderTextColor={'#A9A9AC'}
              secureTextEntry={!showPassword}
            />
          )}
          name="password"
          rules={{required: true}}
        />
        <TouchableOpacity
          onPress={togglePasswordVisibility}
          activeOpacity={0.8}>
          <Image
            source={showPassword ? IconAssets.eye_open : IconAssets.eye_closed}
          />
        </TouchableOpacity>
      </View>
      <Text style={styles.label}>Confirm Password</Text>
      <View style={styles.inputContainer}>
        <Image source={IconAssets.password} />
        <Controller
          control={control}
          render={({field: {onChange, onBlur, value}}) => (
            <TextInput
              style={styles.input}
              onBlur={onBlur}
              onChangeText={value => {
                onChange(value);
                checkPasswordMatch(value);
              }}
              value={value}
              placeholder="Confirm Password"
              placeholderTextColor={'#A9A9AC'}
              secureTextEntry={!showPassword}
            />
          )}
          name="confirmPassword"
          rules={{required: true}}
        />
        <TouchableOpacity
          onPress={togglePasswordVisibility}
          activeOpacity={0.8}>
          <Image
            source={showPassword ? IconAssets.eye_open : IconAssets.eye_closed}
          />
        </TouchableOpacity>
      </View>
      {!passwordMatch && (
        <Text style={styles.errorText}>Passwords do not match</Text>
      )}
      <TouchableOpacity
        style={
          isSubmitDisabled
            ? styles.registerButtonDisabled
            : styles.registerButton
        }
        onPress={handleSubmit(data =>
          onSubmit(data.name, data.email, data.phoneNumber, data.password),
        )}
        disabled={isSubmitDisabled}>
        <Text style={styles.registerButtonText}>Register</Text>
      </TouchableOpacity>
      <View style={styles.loginContainer}>
        <Text style={styles.haveAccountText}>Already have an account? </Text>
        <TouchableOpacity onPress={handleClickLogin} activeOpacity={0.8}>
          <Text style={styles.loginButtonText}>Login</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default RegisterComponent;
