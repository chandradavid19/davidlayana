import React from 'react';
import {StatusBar, Platform, SafeAreaView, ScrollView} from 'react-native';
import {styles} from './Screen.style';

const Screen = props => {
  const isAndroid = Platform.OS === 'android';
  const statusBarStyle = isAndroid ? 'light-content' : 'default';

  return (
    <SafeAreaView
      style={[
        styles.safeContainer,
        {backgroundColor: props.statusBarColor || 'white'},
      ]}>
      <StatusBar barStyle={statusBarStyle} translucent={true} />
      <ScrollView
        contentContainerStyle={styles.scrollContainer}
        showsVerticalScrollIndicator={false}>
        {props.children}
      </ScrollView>
    </SafeAreaView>
  );
};

export default Screen;
