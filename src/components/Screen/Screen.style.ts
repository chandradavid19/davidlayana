import {StyleSheet, Platform} from 'react-native';

const isAndroid = Platform.OS === 'android';

export const styles = StyleSheet.create({
  safeContainer: {
    flex: 1,
    padding: !isAndroid ? 18 : 0,
  },
  scrollContainer: {
    flexGrow: 1,
  },
});
