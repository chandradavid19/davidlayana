import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  label: {
    color: 'black',
    margin: 20,
    marginLeft: 0,
    marginBottom: 10,
  },
  button: {
    marginTop: 40,
    color: 'black',
    height: 40,
    backgroundColor: '#ec5990',
    borderRadius: 4,
  },
  container: {
    justifyContent: 'center',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#D1D5DB',
    borderRadius: 8,
    marginBottom: 10,
    paddingHorizontal: 8,
  },
  input: {
    flex: 1,
    padding: 10,
    color: '#000',
  },
  icon: {
    marginHorizontal: 10,
  },
  forgotPasswordContainer: {
    alignItems: 'flex-end',
  },
  forgotText: {
    color: '#003566',
    fontWeight: '600',
  },
  loginButton: {
    backgroundColor: '#003566',
    borderRadius: 8,
    paddingVertical: 17,
    width: '100%',
    marginTop: 20,
  },
  loginButtonDisabled: {
    backgroundColor: '#91a3b0',
    borderRadius: 8,
    paddingVertical: 17,
    width: '100%',
    marginTop: 20,
  },
  loginButtonText: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
    textAlign: 'center',
  },
  registerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  haveAccountText: {
    textAlign: 'center',
    color: '#6B7280',
    fontSize: 14,
    fontWeight: '600',
  },
  registerButtonText: {
    textAlign: 'center',
    color: '#003566',
    fontSize: 14,
    fontWeight: '600',
  },
});
