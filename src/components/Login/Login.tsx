import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  TextInput,
  Image,
  Animated,
  TouchableOpacity,
} from 'react-native';
import {useForm, Controller} from 'react-hook-form';

import {styles} from './Login.style';

import {IconAssets} from '../../assets/icons';

interface LoginComponentProps {
  onSubmit: (email: string, password: string) => void;
  handleClickForgotPassword: () => void;
  handleClickRegister: () => void;
  translateY3: Animated.Value;
  isSafeToReset: boolean;
}

const LoginComponent: React.FC<LoginComponentProps> = ({
  onSubmit,
  handleClickForgotPassword,
  handleClickRegister,
  translateY3,
  isSafeToReset,
}) => {
  const {control, handleSubmit, formState, reset} = useForm({
    defaultValues: {
      email: '',
      password: '',
    },
  });

  const isSubmitDisabled =
    !formState.isValid ||
    !formState.dirtyFields.email ||
    !formState.dirtyFields.password;

  const [showPassword, setShowPassword] = useState(false);

  const togglePasswordVisibility = () => {
    setShowPassword(prevState => !prevState);
  };

  useEffect(() => {
    if (!isSafeToReset) {
      return;
    }
    reset({email: '', password: ''});
  }, [reset, isSafeToReset]);

  return (
    <Animated.View
      style={[styles.container, {transform: [{translateY: translateY3}]}]}>
      <Text style={styles.label}>Email</Text>
      <View style={styles.inputContainer}>
        <Image source={IconAssets.email} />
        <Controller
          control={control}
          render={({field: {onChange, onBlur, value}}) => (
            <TextInput
              style={styles.input}
              onBlur={onBlur}
              onChangeText={value => onChange(value)}
              value={value}
              placeholder="Email"
              placeholderTextColor={'#A9A9AC'}
            />
          )}
          name="email"
          rules={{required: true}}
        />
      </View>
      <Text style={styles.label}>Password</Text>
      <View style={styles.inputContainer}>
        <Image source={IconAssets.password} />
        <Controller
          control={control}
          render={({field: {onChange, onBlur, value}}) => (
            <TextInput
              style={styles.input}
              onBlur={onBlur}
              onChangeText={value => onChange(value)}
              value={value}
              placeholder="Password"
              placeholderTextColor={'#A9A9AC'}
              secureTextEntry={!showPassword}
            />
          )}
          name="password"
          rules={{required: true}}
        />
        <TouchableOpacity
          onPress={togglePasswordVisibility}
          activeOpacity={0.8}>
          <Image
            source={showPassword ? IconAssets.eye_open : IconAssets.eye_closed}
          />
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        onPress={handleClickForgotPassword}
        style={styles.forgotPasswordContainer}
        activeOpacity={0.8}>
        <Text style={styles.forgotText}>Forgot password?</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={
          isSubmitDisabled ? styles.loginButtonDisabled : styles.loginButton
        }
        onPress={handleSubmit(data => onSubmit(data.email, data.password))}
        disabled={isSubmitDisabled}>
        <Text style={styles.loginButtonText}>Login</Text>
      </TouchableOpacity>
      <View style={styles.registerContainer}>
        <Text style={styles.haveAccountText}>Don't have an account?</Text>
        <TouchableOpacity onPress={handleClickRegister}>
          <Text style={styles.registerButtonText}> Register</Text>
        </TouchableOpacity>
      </View>
    </Animated.View>
  );
};

export default LoginComponent;
