export const IconAssets = {
  email: require('./email.png'),
  password: require('./password.png'),
  eye_closed: require('./eye_closed.png'),
  eye_open: require('./eye_open.png'),
  name: require('./name.png'),
};
