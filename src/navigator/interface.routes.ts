import {
  NavigationHelpers,
  NavigationProp,
  ParamListBase,
  StackActionHelpers,
  StackNavigationState,
} from '@react-navigation/native';
import {
  StackNavigationEventMap,
  StackNavigationOptions,
} from '@react-navigation/stack';

export declare type StackNavigationHelpers = NavigationHelpers<
  ParamListBase,
  StackNavigationEventMap
> &
  StackActionHelpers<ParamListBase>;

export declare type StackNavigationProp<
  ParamList extends ParamListBase,
  RouteName extends keyof ParamList = keyof ParamList,
  NavigatorID extends string | undefined = undefined,
> = NavigationProp<
  ParamList,
  RouteName,
  NavigatorID,
  StackNavigationState<ParamList>,
  StackNavigationOptions,
  StackNavigationEventMap
> &
  StackActionHelpers<ParamList>;

export type RootStackParamList = {
  Login: undefined;
  Register: undefined;
  Homepage: undefined;
};
