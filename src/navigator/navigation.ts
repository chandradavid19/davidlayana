import {createNavigationContainerRef} from '@react-navigation/native';
import {RootStackParamList} from './interface.routes';

export const navigationRef = createNavigationContainerRef<RootStackParamList>();

export const navigate = (
  name: keyof RootStackParamList,
  params?: RootStackParamList[keyof RootStackParamList],
) => {
  if (navigationRef.isReady()) {
    navigationRef.navigate(name, params);
  }
};

interface ResetProps {
  index: number;
  routes: {
    name: keyof RootStackParamList;
    params?: RootStackParamList[keyof RootStackParamList];
  }[];
}

export const reset = ({index, routes}: ResetProps) => {
  if (navigationRef.isReady()) {
    navigationRef.reset({
      index,
      routes,
    });
  }
};
