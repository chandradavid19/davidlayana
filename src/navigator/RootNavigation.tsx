import React, {useEffect, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import {navigationRef, navigate} from './navigation';

import Login from '../screens/Login/Login.screen';
import Register from '../screens/Register/Register.screen';
import Homepage from '../screens/Homepage/Homepage.screen';

import useDatabase from '../utils/useDatabase';

const RootNavigation = () => {
  const Stack = createStackNavigator();
  const [isSessionChecked, setSessionChecked] = useState(false);
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const db = useDatabase();

  useEffect(() => {
    const checkUserSession = async () => {
      try {
        const result = await new Promise((resolve, reject) => {
          db.transaction(tx => {
            tx.executeSql(
              'SELECT COUNT(*) as count FROM Sessions',
              [],
              (_tx, results) => {
                const count = results.rows.item(0).count;
                resolve(count > 0);
              },
              error => {
                reject(error);
              },
            );
          });
        });

        setIsLoggedIn(!!result);

        if (result) {
          navigate('Homepage');
        } else {
          navigate('Login');
        }
      } catch (error) {
        console.error('Error checking user session:', error);
      } finally {
        setSessionChecked(true);
      }
    };

    checkUserSession();
  }, [db]);

  if (!isSessionChecked) {
    return null;
  }

  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator
        initialRouteName={isLoggedIn ? 'Homepage' : 'Login'}
        screenOptions={{
          animationEnabled: true,
          animationTypeForReplace: 'push',
          gestureEnabled: true,
          headerShown: false,
        }}>
        <Stack.Group
          screenOptions={{
            animationEnabled: true,
            animationTypeForReplace: 'push',
          }}>
          <Stack.Screen
            name="Login"
            component={Login}
            options={{
              headerShown: false,
              ...TransitionPresets.SlideFromRightIOS,
            }}
          />
          <Stack.Screen
            name="Register"
            component={Register}
            options={{
              headerShown: false,
              ...TransitionPresets.SlideFromRightIOS,
            }}
          />
          <Stack.Screen
            name="Homepage"
            component={Homepage}
            options={{
              headerShown: false,
              ...TransitionPresets.SlideFromRightIOS,
            }}
          />
        </Stack.Group>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RootNavigation;
